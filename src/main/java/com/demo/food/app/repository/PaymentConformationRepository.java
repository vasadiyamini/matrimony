package com.demo.food.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.food.app.entity.PaymentConformation;

@Repository
public interface PaymentConformationRepository extends JpaRepository<PaymentConformation, Integer> {

	List<PaymentConformation> findByCartCartIdAndPaymentStatus(int cartId,String paymentStatus);
	Optional <PaymentConformation> findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(int cartId ,int foodId
			,String paymentStatus);
}
