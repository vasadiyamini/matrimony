package com.demo.food.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.food.app.entity.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Integer> {

	Optional<Vendor> findByEmail(String emailId);

}
