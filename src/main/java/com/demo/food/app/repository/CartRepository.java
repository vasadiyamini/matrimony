package com.demo.food.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.food.app.entity.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {

}
