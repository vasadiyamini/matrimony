package com.demo.food.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.food.app.entity.FoodCourt;

@Repository
public interface FoodCourtRepository extends JpaRepository<FoodCourt, Integer> {
	List<FoodCourt> findByFoodNameContainingIgnoreCaseOrFoodTypeContainingIgnoreCaseOrFoodPriceBetweenOrVendorHotelName(
			String foodName, String foodType, Integer minPrice, Integer maxPrice, String hotelName);
	 List<FoodCourt> findByFoodNameContainingIgnoreCaseAndVendorVendorId(String foodName, int vendorId);
	 List<FoodCourt> findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCase(String foodName, String foodType);
	 List<FoodCourt> findByFoodNameContainingIgnoreCaseAndVendorHotelName(String foodName,String hotelName);
	 List<FoodCourt> findByFoodTypeContainingIgnoreCaseAndVendorHotelName(String foodType,String hotelName);
	 List<FoodCourt> findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCaseAndVendorHotelName(
				String foodName, String foodType, String hotelName);
}
