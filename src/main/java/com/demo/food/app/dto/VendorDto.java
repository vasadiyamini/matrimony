package com.demo.food.app.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VendorDto {
	@NotBlank(message = "name is requred field")
	private String vendorName;
	@NotBlank(message = "mail is requred field")
	@Email(message = "invalid Email Id")
	private String email;
	@Size(min = 10, max = 12)
	private String vendorMobileNum;
	@NotBlank(message = "name is requred field")
	private String hotelName;
}
