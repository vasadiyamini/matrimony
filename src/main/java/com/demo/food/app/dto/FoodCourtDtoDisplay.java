package com.demo.food.app.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FoodCourtDtoDisplay {
	private String foodName;
	private int foodPrice;
	private String foodType;
	private String description;
	private String hotelName;
	private int foodId;
}
