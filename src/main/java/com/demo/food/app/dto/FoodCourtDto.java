package com.demo.food.app.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FoodCourtDto {

	@NotBlank(message = "food name is requred field")
	private String foodName;
	@NotNull(message = "food price is requred field")
	private int foodPrice;
	private String foodType;
	private String description;
}
