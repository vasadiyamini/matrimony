package com.demo.food.app.service;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CustomerDto;
import com.demo.food.app.dto.LoginDto;
import com.demo.food.app.dto.VendorDto;

public interface RegistrationService {

	ApiResponse addVendor(VendorDto vendorDto);

	ApiResponse addCustomer(CustomerDto customerDto);

	ApiResponse addLoginn(LoginDto logindto);

}
