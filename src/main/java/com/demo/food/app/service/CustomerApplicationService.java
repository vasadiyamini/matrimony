package com.demo.food.app.service;

import java.util.List;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CartDto;
import com.demo.food.app.dto.FoodCourtDtoDisplay;

public interface CustomerApplicationService {

	List<FoodCourtDtoDisplay> getFoods( String hotelName, String foodName, String foodType,
			Integer minPrice, Integer maxPrice);

	ApiResponse addCarts(int customerid, int foodid, CartDto cartDto);

	ApiResponse addPayments(int customerid);

	ApiResponse deleteFoodcart(int customerid, int foodid);

}
