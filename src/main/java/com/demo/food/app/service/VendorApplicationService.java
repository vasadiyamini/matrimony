package com.demo.food.app.service;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.FoodCourtDto;

public interface VendorApplicationService {

	ApiResponse addFoodCou(int vendorid, FoodCourtDto foodCourtDto);

	ApiResponse deleteFoodCou(int vendorid, int foodid);

	ApiResponse updateFoodCou(int vendorid, int foodid, FoodCourtDto foodCourtDto);

}
