package com.demo.food.app.serviceimp;

import org.springframework.stereotype.Service;
import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.FoodCourtDto;
import com.demo.food.app.entity.FoodCourt;
import com.demo.food.app.entity.Vendor;
import com.demo.food.app.exception.ResourceConflictExists;
import com.demo.food.app.exception.ResourceNotFound;
import com.demo.food.app.exception.UnUuthorizedUser;
import com.demo.food.app.repository.FoodCourtRepository;
import com.demo.food.app.repository.VendorRepository;
import com.demo.food.app.service.VendorApplicationService;

import java.util.List;


@Service
public class VendorApplicationServiceImp implements VendorApplicationService {
	private final VendorRepository vendorRepository;
	private final FoodCourtRepository foodCourtRepository;

	public VendorApplicationServiceImp(VendorRepository vendorRepository, FoodCourtRepository foodCourtRepository) {
		this.vendorRepository = vendorRepository;
		this.foodCourtRepository = foodCourtRepository;
	}
private String message ="In valid vendor";
	@Override
	public ApiResponse addFoodCou(int vendorid, FoodCourtDto foodCourtDto) {

		Vendor vendor = vendorRepository.findById(vendorid).orElseThrow(() -> new ResourceNotFound(message));
		if (vendor.isLogin()) {
			FoodCourt foodCourt = FoodCourt.builder().foodName(foodCourtDto.getFoodName())
					.foodPrice(foodCourtDto.getFoodPrice()).description(foodCourtDto.getDescription())
					.foodType(foodCourtDto.getFoodType()).vendor(vendor).build();
			List<FoodCourt> foodCourt1 = foodCourtRepository.findByFoodNameContainingIgnoreCaseAndVendorVendorId(foodCourt.getFoodName(),vendorid);
			if (foodCourt1.isEmpty()) {
				foodCourtRepository.save(foodCourt);
				ApiResponse api = new ApiResponse();
				api.setStatus(200l);
				api.setMessage("Food successfully added");
				return api;
			} else {
				for (FoodCourt x : foodCourt1) {
					if (x.getDescription().equals(foodCourt.getDescription())) {
						throw new ResourceConflictExists("this food item alredy present in the hotel");
					}
				}
				foodCourtRepository.save(foodCourt);
				ApiResponse api = new ApiResponse();
				api.setStatus(200l);
				api.setMessage("Food successfully added");
				return api;
			}

		}

		else
			throw new ResourceConflictExists("vendor should be login to add food ");
	}

	@Override
	public ApiResponse deleteFoodCou(int vendorid, int foodid) {
		Vendor vendor = vendorRepository.findById(vendorid).orElseThrow(() -> new ResourceNotFound(message));
		FoodCourt foodCourt = foodCourtRepository.findById(foodid)
				.orElseThrow(() -> new ResourceNotFound("In valid food court"));
		Vendor foodCourtVendor = foodCourt.getVendor();
		int foodCourtvendorId = foodCourtVendor.getVendorId();
		if (vendorid == foodCourtvendorId) {
			if (vendor.isLogin()) {
				foodCourt.setVendor(null);
				foodCourtRepository.deleteById(foodid);
				ApiResponse api = new ApiResponse();
				api.setStatus(200l);
				api.setMessage("Food Court successfully deleted");
				return api;
			} else
				throw new ResourceConflictExists("vendor should be login to delete foodCourt");
		} else
			throw new UnUuthorizedUser("vendor not related to this food item ");
	}

	@Override
	public ApiResponse updateFoodCou(int vendorid, int foodid, FoodCourtDto foodCourtDto) {

		Vendor vendor = vendorRepository.findById(vendorid).orElseThrow(() -> new ResourceNotFound(message));
		FoodCourt foodCourt = foodCourtRepository.findById(foodid)
				.orElseThrow(() -> new ResourceNotFound("In valid food court"));
		Vendor foodCourtVendor = foodCourt.getVendor();
		int foodCourtvendorId = foodCourtVendor.getVendorId();
		if (vendorid == foodCourtvendorId) {
			if (vendor.isLogin()) {
				foodCourt.setFoodName(foodCourtDto.getFoodName());
				foodCourt.setFoodPrice(foodCourtDto.getFoodPrice());
				foodCourt.setDescription(foodCourtDto.getDescription());
				foodCourt.setFoodType(foodCourtDto.getFoodType());
				foodCourtRepository.save(foodCourt);
				ApiResponse api = new ApiResponse();
				api.setStatus(200l);
				api.setMessage("Food Court successfully updated");
				return api;
			} else
				throw new ResourceConflictExists("vendor should be login to update foodCourt");
		} else
			throw new UnUuthorizedUser("vendor not related to this food item ");
	}

}
