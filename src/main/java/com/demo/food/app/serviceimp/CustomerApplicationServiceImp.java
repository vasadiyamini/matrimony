package com.demo.food.app.serviceimp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CartDto;
import com.demo.food.app.dto.FoodCourtDtoDisplay;
import com.demo.food.app.entity.Cart;
import com.demo.food.app.entity.Customer;
import com.demo.food.app.entity.FoodCourt;
import com.demo.food.app.entity.PaymentConformation;
import com.demo.food.app.exception.ResourceConflictExists;
import com.demo.food.app.exception.ResourceNotFound;
import com.demo.food.app.exception.UnUuthorizedUser;
import com.demo.food.app.repository.CartRepository;
import com.demo.food.app.repository.CustomerRepository;
import com.demo.food.app.repository.FoodCourtRepository;
import com.demo.food.app.repository.PaymentConformationRepository;
import com.demo.food.app.service.CustomerApplicationService;

@Service
public class CustomerApplicationServiceImp implements CustomerApplicationService {
	private final CustomerRepository customerRepository;
	private final FoodCourtRepository foodCourtRepository;
	private final CartRepository cartRepository;
	private final PaymentConformationRepository paymentConformationRepository;

	public CustomerApplicationServiceImp(CustomerRepository customerRepository, FoodCourtRepository foodCourtRepository,
			CartRepository cartRepository,PaymentConformationRepository paymentConformationRepository) {

		this.customerRepository = customerRepository;
		this.foodCourtRepository = foodCourtRepository;
		this.cartRepository = cartRepository;
		this.paymentConformationRepository = paymentConformationRepository;
	}
private String message ="In valid Customer";
private String paymentStatus = "Pending";
	@Override
	public List<FoodCourtDtoDisplay> getFoods(String hotelName, String foodName, String foodType, Integer minPrice,
			Integer maxPrice) {
		List<FoodCourt> foodCourts;
		if (foodName != null && foodType != null && hotelName != null) {
			foodCourts = foodCourtRepository
					.findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCaseAndVendorHotelName(foodName,
							foodType, hotelName);
		} else if (foodName != null && foodType != null) {
			foodCourts = foodCourtRepository.findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCase(foodName,
					foodType);
		} else if (hotelName != null && foodName != null) {
			foodCourts = foodCourtRepository.findByFoodNameContainingIgnoreCaseAndVendorHotelName(foodName, hotelName);
		} else if (foodType != null && hotelName != null) {
			foodCourts = foodCourtRepository.findByFoodTypeContainingIgnoreCaseAndVendorHotelName(foodType, hotelName);
		}

		else {
			foodCourts = foodCourtRepository
					.findByFoodNameContainingIgnoreCaseOrFoodTypeContainingIgnoreCaseOrFoodPriceBetweenOrVendorHotelName(
							foodName, foodType, minPrice, maxPrice, hotelName);
		}
		if (foodCourts.isEmpty()) {
			throw new ResourceConflictExists("no results for this search");
		} else {
			return foodCourts.stream()
					.map(foodCourt -> FoodCourtDtoDisplay.builder().foodName(foodCourt.getFoodName())
							.foodPrice(foodCourt.getFoodPrice()).foodType(foodCourt.getFoodType())
							.foodId(foodCourt.getFoodId()).description(foodCourt.getDescription())
							.hotelName(foodCourt.getVendor().getHotelName()).build())
					.toList();
		}

	}

	@Override
	public ApiResponse addCarts(int customerid, int foodid, CartDto cartDto) {
		Customer customer = customerRepository.findById(customerid)
				.orElseThrow(() -> new ResourceNotFound(message));
		FoodCourt foodCourt1 = foodCourtRepository.findById(foodid)
				.orElseThrow(() -> new ResourceNotFound("In valid food court"));
		if (cartDto.getQuantity() == 0) {
			throw new ResourceConflictExists("Quntity must be greater the value of 0");
		}
		
		if (customer.isLogin()) {
			Optional<Cart> cart = cartRepository.findById(customerid);
			Cart cart1 = cart.get();
			int tPrice = foodCourt1.getFoodPrice() * cartDto.getQuantity() + cart1.getTotalPrice();
			int tQuntity = cartDto.getQuantity() + cart1.getQuantity();

			List<FoodCourt> foodCourts1 = cart1.getFoodCourt();
			if(foodCourts1 == null) {
				foodCourts1 = new ArrayList<>();
			}
			foodCourts1.add(foodCourt1);
			cart1.setTotalPrice(tPrice);
			cart1.setPaymentStatus(paymentStatus);
			cart1.setPaymentMethod(cartDto.getPaymentMethod());
			cart1.setQuantity(tQuntity);
			cartRepository.save(cart1);
			PaymentConformation paymentConformation =PaymentConformation.builder().cart(cart1).foodCourt(foodCourt1)
					.paymentStatus(paymentStatus).price(foodCourt1.getFoodPrice()).quantity(cartDto.getQuantity()).build();
			paymentConformationRepository.save(paymentConformation);
			ApiResponse api = new ApiResponse();
			api.setStatus(200l);
			api.setMessage("Food successfully added into Cart");
			return api;

		}

		else
			throw new UnUuthorizedUser("Customer Should be login to add items into cart");
	}

	@Override
	public ApiResponse addPayments(int customerid) {
		Customer customer = customerRepository.findById(customerid)
				.orElseThrow(() -> new ResourceNotFound(message));
		Optional<Cart> cart1 = cartRepository.findById(customerid);
		Cart cart = cart1.get();
		if (customer.isLogin()) {
			cart.setTotalPrice(0);
			cart.setQuantity(0);
			cart.getPaymentStatus();
			cart.setPaymentMethod(null);
			cart.setFoodCourt(null);
			cart.setPaymentStatus(null);
			cartRepository.save(cart);
			
			List<PaymentConformation> paymentConformation = paymentConformationRepository.findByCartCartIdAndPaymentStatus(customerid, paymentStatus);
			if(paymentConformation.isEmpty()) {
				throw new ResourceConflictExists("No Food items avilable in Cart");
			}
			for(PaymentConformation x :paymentConformation) {
				x.setPaymentStatus("FINISHED");
				paymentConformationRepository.save(x);
			}
			
			ApiResponse api = new ApiResponse();
			api.setStatus(200l);
			api.setMessage("Payment successfully done");
			return api;
		} else
			throw new UnUuthorizedUser("Customer Should be login to Conform the payment");
	}

	@Override
	public ApiResponse deleteFoodcart(int customerid, int foodid) {
		Customer customer = customerRepository.findById(customerid)
				.orElseThrow(() -> new ResourceNotFound(message));
		if (customer.isLogin()) {
			Cart cart = cartRepository.findById(customerid)
					.orElseThrow(() -> new ResourceNotFound(message));
			FoodCourt foodcourt = foodCourtRepository.findById(foodid)
					.orElseThrow(() -> new ResourceNotFound("There is no such type of food item"));
			PaymentConformation	paymentConformation =paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(customerid, foodid,paymentStatus)
			               .orElseThrow(() -> new ResourceNotFound("This food item is not added into the customer cart"));
			int quantity1 = paymentConformation.getQuantity();
			int price1 = paymentConformation.getPrice();
			int id = paymentConformation.getPaymentId();
			List<FoodCourt> foodCourts = cart.getFoodCourt();
			if (foodCourts.contains(foodcourt)) {
				foodCourts.remove(foodcourt);
				int tQuntity = cart.getQuantity() - quantity1;
				int tPrice = cart.getTotalPrice() - price1;
				cart.setTotalPrice(tPrice);
				cart.setPaymentStatus(paymentStatus);
				cart.setQuantity(tQuntity);
				paymentConformation.setCart(null);
				paymentConformation.setFoodCourt(null);
				paymentConformationRepository.deleteById(id);
				cartRepository.save(cart);
				ApiResponse api = new ApiResponse();
				api.setStatus(200l);
				api.setMessage("Food successfully deleted from the Cart");
				return api;
			} else
				throw new ResourceConflictExists("the food item not added into the customer cart");
		} else
			throw new UnUuthorizedUser("Customer Should be login to delete the item into the cart");
	}

}
