package com.demo.food.app.serviceimp;

import java.util.Optional;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CustomerDto;
import com.demo.food.app.dto.LoginDto;
import com.demo.food.app.dto.VendorDto;
import com.demo.food.app.entity.Cart;
import com.demo.food.app.entity.Customer;
import com.demo.food.app.entity.Vendor;
import com.demo.food.app.exception.ResourceConflictExists;
import com.demo.food.app.exception.UnUuthorizedUser;
import com.demo.food.app.repository.CartRepository;
import com.demo.food.app.repository.CustomerRepository;
import com.demo.food.app.repository.VendorRepository;
import com.demo.food.app.service.RegistrationService;

@Service
public class RegistrationServiceImp implements RegistrationService {
	private final VendorRepository vendorRepository;
	private final CustomerRepository customerRepository;
private final CartRepository cartRepository;
	RegistrationServiceImp(VendorRepository vendorRepository, CustomerRepository customerRepository
			,CartRepository cartRepository) {
		this.vendorRepository = vendorRepository;
		this.customerRepository = customerRepository;
		this.cartRepository = cartRepository; 
	}

	@Override
	public ApiResponse addVendor(VendorDto vendorDto) {
		 Optional<Vendor> mail = vendorRepository.findByEmail(vendorDto.getEmail());
		if (mail.isPresent())
			throw new ResourceConflictExists("Vendor already registred");
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String pwd = RandomStringUtils.random(7, characters);
		Vendor vendor = Vendor.builder().hotelName(vendorDto.getHotelName()).email(vendorDto.getEmail())
				.vendorMobileNum(vendorDto.getVendorMobileNum()).vendorName(vendorDto.getVendorName()).user("Vendor")
				.password(pwd).login(false).build();
		vendorRepository.save(vendor);
		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Sucessfully vendor added");
		return api;

	}

	@Override
	public ApiResponse addCustomer(CustomerDto customerDto) {
		Optional<Customer> mail = customerRepository.findByEmail(customerDto.getEmail());
		if (mail.isPresent())
			throw new ResourceConflictExists("Customer already registred");
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String pwd = RandomStringUtils.random(7, characters);
		Customer customer = Customer.builder().customerName(customerDto.getCustomerName())
				.customerMobileNum(customerDto.getCustomerMobileNum()).email(customerDto.getEmail()).login(false)
				.password(pwd).user("Customer").build();
		Cart cart = Cart.builder().customer(customer).build(); 
		
		
		customerRepository.save(customer);
		cartRepository.save(cart);

		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Sucessfully Customer added");
		return api;
	}

	@Override
	public ApiResponse addLoginn(LoginDto logindto) {
		Optional<Vendor> mail = vendorRepository.findByEmail(logindto.getEmailId());
		if (mail.isPresent()) {
			Optional<Vendor> ven = vendorRepository.findByEmail(logindto.getEmailId());
			Vendor vendor = ven.get();
			if (vendor.getPassword().equals(logindto.getPassword())) {
				vendor.setLogin(true);
				vendorRepository.save(vendor);
				ApiResponse api = new ApiResponse();
				api.setStatus(200l);
				api.setMessage("Sucessfully vendor login");
				return api;
			} else
				throw new ResourceConflictExists("Correct the password");
		} else {
			Customer cus = customerRepository.findByEmail(logindto.getEmailId())
					.orElseThrow(() -> new UnUuthorizedUser("User not exits"));
			if (cus.getPassword().equals(logindto.getPassword())) {
				cus.setLogin(true);
				customerRepository.save(cus);
				ApiResponse api = new ApiResponse();
				api.setStatus(200l);
				api.setMessage("Sucessfully Customer login");
				return api;
			} else
				throw new ResourceConflictExists("Correct the password");
		}

	}
}
