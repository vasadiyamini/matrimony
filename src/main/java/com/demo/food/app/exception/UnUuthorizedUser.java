package com.demo.food.app.exception;

public class UnUuthorizedUser extends FoodAppGlobalException{


	private static final long serialVersionUID = 1L;
	public UnUuthorizedUser() {
		super("UnAuthorised user", GlobalErrorCode.ERROR_UNAUTHOURIZED_USER);
	}

	public UnUuthorizedUser(String message) {
		super(message, GlobalErrorCode.ERROR_UNAUTHOURIZED_USER);
	}
}
