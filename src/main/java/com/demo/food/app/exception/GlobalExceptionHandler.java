package com.demo.food.app.exception;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	 @Override
		protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
				HttpHeaders headers, HttpStatusCode status, WebRequest request) {
			Map<String, String> errors = new HashMap<>();
			ex.getBindingResult().getAllErrors().forEach(error -> {
				String fieldName = ((FieldError) error).getField();
				String message = error.getDefaultMessage();
				errors.put(fieldName, message);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
		}
	 @ExceptionHandler(FoodAppGlobalException.class)
		protected ResponseEntity<ErrorResponse> handleGlobalException(
				FoodAppGlobalException foodAppGlobalException, Locale locale) {
			return ResponseEntity.badRequest().body(ErrorResponse.builder().code(foodAppGlobalException.getStatus().value())
					.message(foodAppGlobalException.getMessage()).build());
		}
	 @ExceptionHandler(DataIntegrityViolationException.class)
		public ResponseEntity<ErrorResponse> handleDataIntegrityViolation(DataIntegrityViolationException ex) {
	 
			return ResponseEntity.badRequest().body(
					ErrorResponse.builder().code(HttpStatus.ALREADY_REPORTED.value()).message("Resource already exist").build());
		}
	 
}
