package com.demo.food.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CustomerDto;
import com.demo.food.app.dto.LoginDto;
import com.demo.food.app.dto.VendorDto;
import com.demo.food.app.service.RegistrationService;

import jakarta.validation.Valid;

@RestController
@RequestMapping
public class RegistrationController {
	private final RegistrationService registrationService;

	RegistrationController(RegistrationService registrationService) {
		this.registrationService = registrationService;
	}

	@PostMapping("/vendor")

	public ResponseEntity<ApiResponse> addVendorRegistration(@Valid @RequestBody VendorDto vendorDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(registrationService.addVendor(vendorDto));
	}

	@PostMapping("/customer")

	public ResponseEntity<ApiResponse> addCustomerRegistration(@Valid @RequestBody CustomerDto customerDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(registrationService.addCustomer(customerDto));
	}

	@PostMapping("/login")
	public ResponseEntity<ApiResponse> addLogin(@RequestBody LoginDto logindto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(registrationService.addLoginn(logindto));

	}

}
