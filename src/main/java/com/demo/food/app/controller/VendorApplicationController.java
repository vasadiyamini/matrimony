package com.demo.food.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.FoodCourtDto;

import com.demo.food.app.service.VendorApplicationService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/vendorrecipes")
public class VendorApplicationController {

	private final VendorApplicationService vendorApplicationService;

	VendorApplicationController(VendorApplicationService vendorApplicationService) {
		this.vendorApplicationService = vendorApplicationService;
	}

	@PostMapping("/{vendorid}")

	public ResponseEntity<ApiResponse> addFoodCourt(@PathVariable int vendorid,
			@Valid @RequestBody FoodCourtDto foodCourtDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(vendorApplicationService.addFoodCou(vendorid, foodCourtDto));
	}

	@DeleteMapping("/{vendorid}/{foodid}")

	public ResponseEntity<ApiResponse> deleteFoodCourt(@PathVariable int vendorid, @PathVariable int foodid) {
		return ResponseEntity.status(HttpStatus.OK).body(vendorApplicationService.deleteFoodCou(vendorid, foodid));
	}

	@PutMapping("/{vendorid}/{foodid}")

	public ResponseEntity<ApiResponse> updateFoodCourt(@PathVariable int vendorid, @PathVariable int foodid,
			@Valid @RequestBody FoodCourtDto foodCourtDto) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(vendorApplicationService.updateFoodCou(vendorid, foodid, foodCourtDto));
	}

}
