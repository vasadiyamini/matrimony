package com.demo.food.app.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CartDto;
import com.demo.food.app.dto.FoodCourtDtoDisplay;
import com.demo.food.app.service.CustomerApplicationService;

import jakarta.validation.Valid;

@RestController
@RequestMapping
public class CustomerApplicationController {

	private final CustomerApplicationService customerApplicationService;

	public CustomerApplicationController(CustomerApplicationService customerApplicationService) {
		this.customerApplicationService = customerApplicationService;
	}

	@GetMapping("/foodmenu")
	public ResponseEntity<List<FoodCourtDtoDisplay>> getFoods(
			@RequestParam(required = false) String hotelName, @RequestParam(required = false) String foodName,
			@RequestParam(required = false) String foodType, @RequestParam(required = false) Integer minPrice,
			@RequestParam(required = false) Integer maxPrice) {
		return ResponseEntity.status(HttpStatus.OK).body(
				customerApplicationService.getFoods( hotelName, foodName, foodType, minPrice, maxPrice));
	}

	@PostMapping("/cart/{customerid}/{foodid}")

	public ResponseEntity<ApiResponse> addCart(@PathVariable int customerid, @PathVariable int foodid,
			@Valid @RequestBody CartDto cartDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(customerApplicationService.addCarts(customerid, foodid, cartDto));
	}

	@PostMapping("/payment/{customerid}")

	public ResponseEntity<ApiResponse> addPayment(@PathVariable int customerid) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(customerApplicationService.addPayments(customerid));
	}
	@DeleteMapping("cart/{customerid}/{foodid}")

	public ResponseEntity<ApiResponse> deleteFoodcart(@PathVariable int customerid, @PathVariable int foodid) {
		return ResponseEntity.status(HttpStatus.OK).body(customerApplicationService.deleteFoodcart(customerid, foodid));
	}
}
