package com.demo.food.app.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Vendor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int vendorId;
	private String vendorName;
	private String email;
	private String vendorMobileNum;
	private String password;
	private String user;
	private String hotelName;
	private boolean login;
	private String createdBy;
	private LocalDateTime createdOn;
	private String updatedBy;
	private LocalDateTime updatedOn;
	@PrePersist
	@PreUpdate
	protected void onUpdateAndCreate() {
		LocalDateTime now = LocalDateTime.now();
		if(createdOn == null) {
			createdOn = now;
			createdBy = "system";
		}
		updatedOn= now;
		updatedBy ="system";
	}
	
}
