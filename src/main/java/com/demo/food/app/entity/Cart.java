package com.demo.food.app.entity;

import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Cart {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cartId;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "customerId")
	private Customer customer;
	@OneToMany(cascade = CascadeType.ALL)
	private List<FoodCourt> foodCourt;
	private int quantity;
	private String paymentMethod;
	private String paymentStatus;
	private int totalPrice;
	private String createdBy;
	private LocalDateTime createdOn;
	private String updatedBy;
	private LocalDateTime updatedOn;
	@PrePersist
	@PreUpdate
	protected void onUpdateAndCreate() {
		LocalDateTime now = LocalDateTime.now();
		if(createdOn == null) {
			createdOn = now;
			createdBy = "system";
		}
		updatedOn= now;
		updatedBy ="system";
	}
}
