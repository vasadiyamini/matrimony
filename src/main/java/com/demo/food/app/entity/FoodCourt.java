package com.demo.food.app.entity;

import java.time.LocalDateTime;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class FoodCourt {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int foodId;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "vendorid")
	private Vendor vendor;
	private String foodName;
	private int foodPrice;
	private String foodType;
	private String description;
	private String createdBy;
	private LocalDateTime createdOn;
	private String updatedBy;
	private LocalDateTime updatedOn;
	@PrePersist
	@PreUpdate
	protected void onUpdateAndCreate() {
		LocalDateTime now = LocalDateTime.now();
		if(createdOn == null) {
			createdOn = now;
			createdBy = "system";
		}
		updatedOn= now;
		updatedBy ="system";
	}
}
