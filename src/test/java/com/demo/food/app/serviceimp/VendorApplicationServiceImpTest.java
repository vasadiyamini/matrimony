package com.demo.food.app.serviceimp;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.FoodCourtDto;
import com.demo.food.app.entity.FoodCourt;
import com.demo.food.app.entity.Vendor;
import com.demo.food.app.exception.ResourceConflictExists;
import com.demo.food.app.exception.ResourceNotFound;
import com.demo.food.app.exception.UnUuthorizedUser;
import com.demo.food.app.repository.FoodCourtRepository;
import com.demo.food.app.repository.VendorRepository;
import com.demo.food.app.service.VendorApplicationService;
import com.demo.food.app.serviceimp.VendorApplicationServiceImp;

class VendorApplicationServiceImpTest {
	@Mock
	private VendorRepository vendorRepository;
	@Mock
	private FoodCourtRepository foodCourtRepository;

	private VendorApplicationService vendorApplicationService;
	private FoodCourtDto foodCourtDto;
	private Vendor vendor;
	private FoodCourt foodCourt;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		vendorApplicationService = new VendorApplicationServiceImp(vendorRepository, foodCourtRepository);
		foodCourtDto = FoodCourtDto.builder().foodName("rice").description("something").build();
		vendor = Vendor.builder().vendorId(1).email("yamini@gmail.com").password("yamini").login(true).build();
		foodCourt = FoodCourt.builder().foodId(1).foodName("biriyani").description("biriyani rice").build();
	}

	@Test
	void testAddFoodCou() {
		FoodCourt foodCourt = FoodCourt.builder().foodName("rice").description("something1").build();
		List<FoodCourt> foodCourtList = new ArrayList<>();
		foodCourtList.add(foodCourt);

		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findByFoodNameContainingIgnoreCaseAndVendorVendorId("biriyani", vendor.getVendorId()))
				.thenReturn(Collections.emptyList());
		ApiResponse response = vendorApplicationService.addFoodCou(1, foodCourtDto);
		assertEquals(200l, response.getStatus().intValue());
		assertEquals("Food successfully added", response.getMessage());

	}
	@Test
	void testAddFoodCou1() {
		Vendor vendor = Vendor.builder().vendorId(1).email("yamini@gmail.com").password("yamini").login(true).build();
		FoodCourt foodCourt = FoodCourt.builder().foodName("rice").description("something1").build();
		List<FoodCourt> foodCourtList = new ArrayList<>();
		foodCourtList.add(foodCourt);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findByFoodNameContainingIgnoreCaseAndVendorVendorId("rice", vendor.getVendorId()))
				.thenReturn(foodCourtList);
		ApiResponse response = vendorApplicationService.addFoodCou(1, foodCourtDto);
		assertEquals(200l, response.getStatus().intValue());
		assertEquals("Food successfully added", response.getMessage());

	}

	@Test
	void testAddFoodCou_InvalidVendor() {
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> vendorApplicationService.addFoodCou(1, foodCourtDto));
		try {
			vendorApplicationService.addFoodCou(1, foodCourtDto);
		} catch (ResourceNotFound e) {
			assertEquals("In valid vendor", e.getMessage());

		}

	}

	@Test
	void testAddFoodCou_VendorNotLogin() {
		vendor.setLogin(false);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		assertThrows(ResourceConflictExists.class, () -> vendorApplicationService.addFoodCou(1, foodCourtDto));
		try {
			vendorApplicationService.addFoodCou(1, foodCourtDto);
		} catch (ResourceConflictExists e) {
			assertEquals("vendor should be login to add food ", e.getMessage());

		}

	}

	@Test
	void testAddFoodCou_AlredyAdded() {
		Vendor vendor = Vendor.builder().vendorId(1).email("yamini@gmail.com").password("yamini").login(true).build();
		FoodCourt foodCourt = FoodCourt.builder().foodName("rice").description("something").build();
		List<FoodCourt> foodCourtList = new ArrayList<>();
		foodCourtList.add(foodCourt);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findByFoodNameContainingIgnoreCaseAndVendorVendorId("rice", vendor.getVendorId()))
				.thenReturn(foodCourtList);
		assertThrows(ResourceConflictExists.class, () -> vendorApplicationService.addFoodCou(1, foodCourtDto));
		try {
			vendorApplicationService.addFoodCou(1, foodCourtDto);
		} catch (ResourceConflictExists e) {
			assertEquals("this food item alredy present in the hotel", e.getMessage());

		}

	}

	@Test
	void testDeleteFoodCou() {
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		ApiResponse response = vendorApplicationService.deleteFoodCou(1, 1);
		assertEquals(200, response.getStatus().intValue());
		assertEquals("Food Court successfully deleted", response.getMessage());

	}

	@Test
	void testDeleteFoodCou_InvalidVendor() {
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.empty());
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		assertThrows(ResourceNotFound.class, () -> vendorApplicationService.deleteFoodCou(1, 1));
		try {
			vendorApplicationService.deleteFoodCou(1, 1);
		} catch (ResourceNotFound e) {
			assertEquals("In valid vendor", e.getMessage());

		}
	}
	@Test
	void testDeleteFoodCou_NotLogin() {
		vendor.setLogin(false);
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		assertThrows(ResourceConflictExists.class, () -> vendorApplicationService.deleteFoodCou(1, 1));
		try {
			vendorApplicationService.deleteFoodCou(1, 1);
		} catch (ResourceConflictExists e) {
			assertEquals("vendor should be login to delete foodCourt", e.getMessage());

		}
	}
	@Test
	void testDeleteFoodCou_InvalidFoodCourt() {
		vendor.setLogin(false);
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> vendorApplicationService.deleteFoodCou(1, 1));
		try {
			vendorApplicationService.deleteFoodCou(1, 1);
		} catch (ResourceNotFound e) {
			assertEquals("In valid food court", e.getMessage());

		}
			}
	@Test
	void testDeleteFoodCou_VendorNotRelated() {
		Vendor vendor1 = Vendor.builder().vendorId(2).build();
		foodCourt.setVendor(vendor1);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		assertThrows(UnUuthorizedUser.class, () -> vendorApplicationService.deleteFoodCou(1, 1));
		try {
			vendorApplicationService.deleteFoodCou(1, 1);
		} catch (UnUuthorizedUser e) {
			assertEquals("vendor not related to this food item ", e.getMessage());

		}
	}
	

	@Test
	void testUpdateFoodCou() {
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		ApiResponse response = vendorApplicationService.updateFoodCou(1, 1, foodCourtDto);
		assertEquals(200, response.getStatus().intValue());
		assertEquals("Food Court successfully updated", response.getMessage());
	}
	@Test
	void testUpdateFoodCou_InvalidVendor() {
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.empty());
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		assertThrows(ResourceNotFound.class, () -> vendorApplicationService.updateFoodCou(1, 1, foodCourtDto));
		try {
			vendorApplicationService.updateFoodCou(1, 1, foodCourtDto);
		} catch (ResourceNotFound e) {
			assertEquals("In valid vendor", e.getMessage());

		}
	}
	@Test
	void testUpdateFoodCou_VendotNotLogin() {
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> vendorApplicationService.updateFoodCou(1, 1, foodCourtDto));
		try {
			vendorApplicationService.updateFoodCou(1, 1, foodCourtDto);
		} catch (ResourceNotFound e) {
			assertEquals("In valid food court", e.getMessage());
	}
		
	}
	@Test
	void testUpdateFoodCou_VendotNotRelated() {
		
		Vendor vendor1 = Vendor.builder().vendorId(2).build();
		foodCourt.setVendor(vendor1);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		assertThrows(UnUuthorizedUser.class, () -> vendorApplicationService.updateFoodCou(1, 1, foodCourtDto));
		try {
			vendorApplicationService.updateFoodCou(1, 1, foodCourtDto);
		} catch (UnUuthorizedUser e) {
			assertEquals("vendor not related to this food item ", e.getMessage());
	}

	}
	@Test
	void testUpdateFoodCou_vendorNotLogin() {
		vendor.setLogin(false);
		foodCourt.setVendor(vendor);
		when(vendorRepository.findById(vendor.getVendorId())).thenReturn(Optional.of(vendor));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		assertThrows(ResourceConflictExists.class, () -> vendorApplicationService.updateFoodCou(1, 1, foodCourtDto));
		try {
			vendorApplicationService.updateFoodCou(1, 1, foodCourtDto);
		} catch (ResourceConflictExists e) {
			assertEquals("vendor should be login to update foodCourt", e.getMessage());
	}
	}
}