package com.demo.food.app.serviceimp;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CartDto;
import com.demo.food.app.dto.FoodCourtDtoDisplay;
import com.demo.food.app.entity.Cart;
import com.demo.food.app.entity.Customer;
import com.demo.food.app.entity.FoodCourt;
import com.demo.food.app.entity.PaymentConformation;
import com.demo.food.app.entity.Vendor;
import com.demo.food.app.exception.ResourceConflictExists;
import com.demo.food.app.exception.ResourceNotFound;
import com.demo.food.app.exception.UnUuthorizedUser;
import com.demo.food.app.repository.CartRepository;
import com.demo.food.app.repository.CustomerRepository;
import com.demo.food.app.repository.FoodCourtRepository;
import com.demo.food.app.repository.PaymentConformationRepository;
import com.demo.food.app.service.CustomerApplicationService;

class CustomerApplicationServiceImpTest {
	@Mock
	private FoodCourtRepository foodCourtRepository;
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private CartRepository cartRepository;
	@Mock
	private PaymentConformationRepository paymentConformationRepository;
	private CustomerApplicationService customerApplicationService;
	private CartDto cartDto;
	private Customer customer;
	private FoodCourt foodCourt;
	private Cart cart;
	private PaymentConformation paymentConformation;
	// private List<FoodCourt> foodCourts1;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		customerApplicationService = new CustomerApplicationServiceImp(customerRepository, foodCourtRepository,
				cartRepository, paymentConformationRepository);
		cartDto = CartDto.builder().quantity(2).paymentMethod("UPI").build();
		customer = Customer.builder().customerId(1).login(true).cart(cart).build();
		
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).build();
		Vendor ven = Vendor.builder().vendorId(1).hotelName("hotel").build();
		foodCourt = FoodCourt.builder().foodId(1).foodPrice(100).foodName("biriyani").foodType("veg").vendor(ven).build();
		paymentConformation = PaymentConformation.builder().quantity(1).price(1).cart(cart).foodCourt(foodCourt)
				.paymentStatus("pending").build();
}

	@Test
	void testGetFoods() {	
		String hotelName = "hotel";
		String foodName ="biriyani";
		String foodType = "veg";
		Integer minPrice= null;
		Integer maxPrice = null;
		when(foodCourtRepository.findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCaseAndVendorHotelName("biriyani",
							"veg", "hotel")).thenReturn(Arrays.asList(foodCourt));
		List<FoodCourtDtoDisplay> result = customerApplicationService.getFoods( hotelName,  foodName,  foodType,  minPrice,
				 maxPrice);
		assertEquals(1,result.size());
	}
	@Test
	void testGetFoods1() {	
		String hotelName = "hotel";
		String foodName ="biriyani";
		String foodType = "veg";
		Integer minPrice= null;
		Integer maxPrice = null;
		when(foodCourtRepository.findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCase("biriyani",
				"veg")).thenReturn(Arrays.asList(foodCourt));
		assertThrows(ResourceConflictExists.class,()->{customerApplicationService.getFoods( hotelName,  foodName,  foodType,  minPrice,
				 maxPrice);});
		try {
			customerApplicationService.getFoods( hotelName,  foodName,  foodType,  minPrice,
					 maxPrice);
		} catch (ResourceConflictExists e) {
			assertEquals("no results for this search", e.getMessage());
		}
	}
	@Test
	void testGetFoods2() {	
		String hotelName = "hotel";
		String foodName ="biriyani";
		String foodType = "veg";
		Integer minPrice= null;
		Integer maxPrice = null;
		when(foodCourtRepository.findByFoodNameContainingIgnoreCaseAndVendorHotelName(foodName, hotelName)).thenReturn(Arrays.asList(foodCourt));
		assertThrows(ResourceConflictExists.class,()->{customerApplicationService.getFoods( hotelName,  foodName,  foodType,  minPrice,
				 maxPrice);});
		try {
			customerApplicationService.getFoods( hotelName,  foodName,  foodType,  minPrice,
					 maxPrice);
		} catch (ResourceConflictExists e) {
			assertEquals("no results for this search", e.getMessage());
		}
	}
	@Test
	void testAddCarts() {
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));

		ApiResponse response = customerApplicationService.addCarts(1, 1, cartDto);
		assertEquals(200, response.getStatus().intValue());
		assertEquals("Food successfully added into Cart", response.getMessage());

	}

	@Test
	void testAddCarts_InvalidCustomer() {

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		assertThrows(ResourceNotFound.class, () -> customerApplicationService.addCarts(1, 1, cartDto));
		try {
			customerApplicationService.addCarts(1, 1, cartDto);
		} catch (ResourceNotFound e) {
			assertEquals("In valid Customer", e.getMessage());
		}
	}

	@Test
	void testAddCarts_InvalidFood() {

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.empty());
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		assertThrows(ResourceNotFound.class, () -> customerApplicationService.addCarts(1, 1, cartDto));
		try {
			customerApplicationService.addCarts(1, 1, cartDto);
		} catch (ResourceNotFound e) {
			assertEquals("In valid food court", e.getMessage());
		}
	}

	@Test
	void testAddCarts_QunFail() {
		cartDto.setQuantity(0);
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		// when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		assertThrows(ResourceConflictExists.class, () -> customerApplicationService.addCarts(1, 1, cartDto));
		try {
			customerApplicationService.addCarts(1, 1, cartDto);
		} catch (ResourceConflictExists e) {
			assertEquals("Quntity must be greater the value of 0", e.getMessage());
		}
	}

	@Test
	void testAddCarts_CusNotLog() {
		customer.setLogin(false);
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		assertThrows(UnUuthorizedUser.class, () -> customerApplicationService.addCarts(1, 1, cartDto));
		try {
			customerApplicationService.addCarts(1, 1, cartDto);
		} catch (UnUuthorizedUser e) {
			assertEquals("Customer Should be login to add items into cart", e.getMessage());
		}
	}

	@Test
	void testAddPayments() {
		paymentConformation = PaymentConformation.builder().cart(cart).foodCourt(foodCourt)
				.paymentStatus("Pending").build();
		 List<PaymentConformation> paymentConformation1= new ArrayList(); 
		 paymentConformation1.add(paymentConformation);
		 List<FoodCourt> foodCourts1 = new ArrayList();
		 foodCourts1.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts1).build();
		//cart.setFoodCourt(foodCourts1);
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(paymentConformationRepository.findByCartCartIdAndPaymentStatus(1, "Pending")).thenReturn(paymentConformation1);
		ApiResponse response = customerApplicationService.addPayments(1);
		assertEquals(200, response.getStatus().intValue());
		assertEquals("Payment successfully done", response.getMessage());
	}
	@Test
	void testAddPayments_NoFoodInCart() {
		
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(paymentConformationRepository.findByCartCartIdAndPaymentStatus(1, "Pending")).thenReturn(Collections.emptyList());
		assertThrows(ResourceConflictExists.class, () -> customerApplicationService.addPayments(1));
		try {
			customerApplicationService.addPayments(1);
		} catch (ResourceConflictExists e) {
			assertEquals("No Food items avilable in Cart", e.getMessage());
		}
	}
	@Test
	void testAddPayments_InvalidCus() {
		
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(paymentConformationRepository.findByCartCartIdAndPaymentStatus(1, "Pending")).thenReturn(Collections.emptyList());
		assertThrows(ResourceNotFound.class, () -> customerApplicationService.addPayments(1));
		try {
			customerApplicationService.addPayments(1);
		} catch (ResourceNotFound e) {
			assertEquals("In valid Customer", e.getMessage());
		}
	}
	@Test
	void testAddPayments_NotLogin() {
		customer.setLogin(false);
			when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
			when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
			when(paymentConformationRepository.findByCartCartIdAndPaymentStatus(1, "Pending")).thenReturn(Collections.emptyList());
		assertThrows(UnUuthorizedUser.class, () -> customerApplicationService.addPayments(1));
		try {
			customerApplicationService.addPayments(1);
		} catch (UnUuthorizedUser e) {
			assertEquals("Customer Should be login to Conform the payment", e.getMessage());
		}
	}

	@Test
	void testDeleteFoodcart() {
		List<FoodCourt> foodCourts = new ArrayList<>();
		foodCourts.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts).build();

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(1, 1, "Pending")).thenReturn(Optional.of(paymentConformation));
		ApiResponse response = customerApplicationService.deleteFoodcart(1, 1);
		assertEquals(200, response.getStatus().intValue());
		assertEquals("Food successfully deleted from the Cart", response.getMessage());
	}
	@Test
	void testDeleteFoodcart_InvalidCustomer() {
		List<FoodCourt> foodCourts = new ArrayList<>();
		foodCourts.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts).build();

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(1, 1, "Pending")).thenReturn(Optional.of(paymentConformation));
		assertThrows(ResourceNotFound.class, () -> customerApplicationService.deleteFoodcart(1, 1));
		try {
			customerApplicationService.deleteFoodcart(1, 1);
		} catch (ResourceNotFound e) {
			assertEquals("In valid Customer", e.getMessage());
		}
	}
	@Test
	void testDeleteFoodcart_InvalidCart() {
		List<FoodCourt> foodCourts = new ArrayList<>();
		foodCourts.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts).build();

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.empty());
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(1, 1, "Pending")).thenReturn(Optional.of(paymentConformation));
		assertThrows(ResourceNotFound.class, () -> customerApplicationService.deleteFoodcart(1, 1));
		try {
			customerApplicationService.deleteFoodcart(1, 1);
		} catch (ResourceNotFound e) {
			assertEquals("In valid Customer", e.getMessage());
		}
	}
	@Test
	void testDeleteFoodcart_InvalidFood() {
		List<FoodCourt> foodCourts = new ArrayList<>();
		foodCourts.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts).build();

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.empty());
		when(paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(1, 1, "Pending")).thenReturn(Optional.of(paymentConformation));
		assertThrows(ResourceNotFound.class, () -> customerApplicationService.deleteFoodcart(1, 1));
		try {
			customerApplicationService.deleteFoodcart(1, 1);
		} catch (ResourceNotFound e) {
			assertEquals("There is no such type of food item", e.getMessage());
		}
	}
	@Test
	void testDeleteFoodcart_NonRelatedFood() {
		List<FoodCourt> foodCourts = new ArrayList<>();
		foodCourts.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts).build();

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(1, 1, "Pending")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> customerApplicationService.deleteFoodcart(1, 1));
		try {
			customerApplicationService.deleteFoodcart(1, 1);
		} catch (ResourceNotFound e) {
			assertEquals("This food item is not added into the customer cart", e.getMessage());
		}
	}
	@Test
	void testDeleteFoodcart_NonAddedCart() {
		List<FoodCourt> foodCourts = new ArrayList<>();
		//foodCourts.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts).build();

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(1, 1, "Pending")).thenReturn(Optional.of(paymentConformation));
		assertThrows(ResourceConflictExists.class, () -> customerApplicationService.deleteFoodcart(1, 1));
		try {
			customerApplicationService.deleteFoodcart(1, 1);
		} catch (ResourceConflictExists e) {
			assertEquals("the food item not added into the customer cart", e.getMessage());
		}
	}
	@Test
	void testDeleteFoodcart_CustomerNotLogin() {
		customer.setLogin(false);
		List<FoodCourt> foodCourts = new ArrayList<>();
		foodCourts.add(foodCourt);
		cart = Cart.builder().cartId(1).totalPrice(1).quantity(1).customer(customer).foodCourt(foodCourts).build();

		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(cartRepository.findById(cart.getCartId())).thenReturn(Optional.of(cart));
		when(foodCourtRepository.findById(foodCourt.getFoodId())).thenReturn(Optional.of(foodCourt));
		when(paymentConformationRepository.findByCartCartIdAndFoodCourtFoodIdAndPaymentStatus(1, 1, "Pending")).thenReturn(Optional.empty());
		assertThrows(UnUuthorizedUser.class, () -> customerApplicationService.deleteFoodcart(1, 1));
		try {
			customerApplicationService.deleteFoodcart(1, 1);
		} catch (UnUuthorizedUser e) {
			assertEquals("Customer Should be login to delete the item into the cart", e.getMessage());
		}
	}

}
