package com.demo.food.app.serviceimp;



//import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CustomerDto;
import com.demo.food.app.dto.LoginDto;
import com.demo.food.app.dto.VendorDto;
import com.demo.food.app.entity.Customer;
import com.demo.food.app.entity.Vendor;
import com.demo.food.app.exception.ResourceConflictExists;
import com.demo.food.app.exception.UnUuthorizedUser;
import com.demo.food.app.repository.CartRepository;
import com.demo.food.app.repository.CustomerRepository;
import com.demo.food.app.repository.VendorRepository;
import com.demo.food.app.service.RegistrationService;
import com.demo.food.app.serviceimp.RegistrationServiceImp;

class RegistrationServiceImpTest {
	@Mock
	private VendorRepository vendorRepository;
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private CartRepository cartRepository;
	private RegistrationService registrationService;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		registrationService = new RegistrationServiceImp(vendorRepository, customerRepository, cartRepository);
	}

	@Test
	void testAddVendor() {

		VendorDto vendorDto = VendorDto.builder().email("yamini@gmail.com").build();
		when(vendorRepository.findByEmail(vendorDto.getEmail())).thenReturn(Optional.empty());
		ApiResponse response = registrationService.addVendor(vendorDto);
		assertEquals(200L, response.getStatus());
		assertEquals("Sucessfully vendor added", response.getMessage());

	}

	@Test
	void testAddVendor_Failure() {

		VendorDto vendorDto = VendorDto.builder().email("yamini@gmail.com").build();
		Vendor vendor = new Vendor();
		when(vendorRepository.findByEmail(vendorDto.getEmail())).thenReturn(Optional.of(vendor));
		// ApiResponse response = registrationService.addVendor(vendorDto);
		assertThrows(ResourceConflictExists.class, () -> registrationService.addVendor(vendorDto));

	}

	@Test
	void testAddCustomer() {
		CustomerDto customerDto = CustomerDto.builder().email("yamini@gmail.com").build();
		when(customerRepository.findByEmail(customerDto.getEmail())).thenReturn(Optional.empty());
		ApiResponse response = registrationService.addCustomer(customerDto);

		assertEquals(200L, response.getStatus());
		assertEquals("Sucessfully Customer added", response.getMessage());
	}

	@Test
	void testAddCustomer_Failure() {
		CustomerDto customerDto = CustomerDto.builder().email("yamini@gmail.com").build();
		Customer customer = new Customer();
		when(customerRepository.findByEmail(customerDto.getEmail())).thenReturn(Optional.of(customer));
		assertThrows(ResourceConflictExists.class, () -> registrationService.addCustomer(customerDto));

	}

	@Test
	void testAddLoginn_VendorSucc() {
		LoginDto logindto = LoginDto.builder().emailId("yamini@gmail.com").password("yamini").build();
		Vendor vendor = Vendor.builder().email("yamini@gmail.com").password("yamini").build();
		// Customer customer =
		// Customer.builder().email("selvi@gmail.com").password("selvi").build();
		when(vendorRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.of(vendor));
		ApiResponse response = registrationService.addLoginn(logindto);
		assertEquals(200l, response.getStatus());
		assertEquals("Sucessfully vendor login", response.getMessage());
		//assertThat(registrationService.addLoginn(logindto)).isEqualTo(response);

	}

	@Test
	void testAddLoginn_VendorFail() {
		LoginDto logindto = LoginDto.builder().emailId("yamini@gmail.com").password("yamini").build();
		Vendor vendor = Vendor.builder().email("yamini@gmail.com").password("yam").build();
		// Customer customer =
		// Customer.builder().email("selvi@gmail.com").password("selvi").build();
		when(vendorRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.empty());
		// ApiResponse response = registrationService.addLoginn(logindto);
		assertThrows(UnUuthorizedUser.class, () -> registrationService.addLoginn(logindto));

	}

	@Test
	void testAddLoginn_VendorFailPassword() {
		LoginDto logindto = LoginDto.builder().emailId("yamini@gmail.com").password("yamini").build();
		Vendor vendor = Vendor.builder().email("yamini@gmail.com").password("yam").build();
		// Customer customer =
		// Customer.builder().email("selvi@gmail.com").password("selvi").build();
		when(vendorRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.of(vendor));
		// ApiResponse response = registrationService.addLoginn(logindto);
		assertThrows(ResourceConflictExists.class, () -> registrationService.addLoginn(logindto));

	}

	@Test
	void testAddLoginn_CustomerSucc() {
		LoginDto logindto = LoginDto.builder().emailId("yamini@gmail.com").password("yamini").build();
		// Vendor vendor =
		// Vendor.builder().email("yamini@gmail.com").password("yamini").build();
		Customer customer = Customer.builder().email("yamini@gmail.com").password("yamini").build();
		when(customerRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.of(customer));
		ApiResponse response = registrationService.addLoginn(logindto);
		assertEquals(200l, response.getStatus());
		assertEquals("Sucessfully Customer login", response.getMessage());
		//assertThat(registrationService.addLoginn(logindto)).isEqualTo(response);

	}

	@Test
	void testAddLoginn_CustomerFail() {
		LoginDto logindto = LoginDto.builder().emailId("yamini@gmail.com").password("yamini").build();
		// Vendor vendor =
		// Vendor.builder().email("yamini@gmail.com").password("yamini").build();
		Customer customer = Customer.builder().email("yamini@gmail.com").password("yamini").build();
		when(customerRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.empty());
		// ApiResponse response = registrationService.addLoginn(logindto);
		assertThrows(UnUuthorizedUser.class, () -> registrationService.addLoginn(logindto));

	}

	@Test
	void testAddLoginn_CustomerFailPassword() {
		LoginDto logindto = LoginDto.builder().emailId("yamini@gmail.com").password("yamini").build();
		// Vendor vendor =
		// Vendor.builder().email("yamini@gmail.com").password("yamini").build();
		Customer customer = Customer.builder().email("yamini@gmail.com").password("yam").build();
		when(customerRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.of(customer));
		// ApiResponse response = registrationService.addLoginn(logindto);
		assertThrows(ResourceConflictExists.class, () -> registrationService.addLoginn(logindto));

	}
}
