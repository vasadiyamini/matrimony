package com.demo.food.app.controller;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CustomerDto;
import com.demo.food.app.dto.LoginDto;
import com.demo.food.app.dto.VendorDto;
import com.demo.food.app.service.RegistrationService;
@ExtendWith(SpringExtension.class)
class RegistrationControllerTest {

	@InjectMocks
	private RegistrationController registrationController;
	@Mock
	private RegistrationService registrationService;
	@Test
	void testAddVendorRegistration() {
		VendorDto vendorDto = VendorDto.builder().email("yamini@gmail.com").hotelName("some").vendorMobileNum("1234567890").vendorName("yamini").build();
		ApiResponse apiResponse = ApiResponse.builder().status(201l).build();
		Mockito.when(registrationService.addVendor(vendorDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity= registrationController.addVendorRegistration(vendorDto);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

	@Test
	void testAddCustomerRegistration() {
		CustomerDto customerDto =CustomerDto.builder().customerMobileNum("7207255572").customerName("yamini").email("yamini@gmail.com").build();
		ApiResponse apiResponse = ApiResponse.builder().status(201l).build();
		Mockito.when(registrationService.addCustomer(customerDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity= registrationController.addCustomerRegistration(customerDto);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

	@Test
	void testAddLogin() {
		LoginDto logindto = LoginDto.builder().emailId("yamini@gmail.com").password("yamini").build();
		ApiResponse apiResponse = ApiResponse.builder().status(201l).build();
		Mockito.when(registrationService.addLoginn(logindto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity= registrationController.addLogin(logindto);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

}
