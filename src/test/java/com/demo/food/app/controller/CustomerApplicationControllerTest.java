package com.demo.food.app.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.CartDto;
import com.demo.food.app.dto.FoodCourtDtoDisplay;
import com.demo.food.app.service.CustomerApplicationService;
@ExtendWith(SpringExtension.class)
class CustomerApplicationControllerTest {
	@InjectMocks
	private CustomerApplicationController customerApplicationController;
	@Mock
	private CustomerApplicationService customerApplicationService;

	@Test
	void testGetFoods() {
		
		String hotelName = "ABC hotel";
		String foodName = "Pizza";
		String foodType = "VEG";
		Integer minPrice =100;
				Integer maxPrice =500;
		FoodCourtDtoDisplay foodCourtDtoDisplay = FoodCourtDtoDisplay.builder().foodName(foodName).foodType(foodType).foodPrice(100)
				.build();
		List<FoodCourtDtoDisplay> foodCourtDtoDisplays = Arrays.asList(foodCourtDtoDisplay);
		when(customerApplicationService.getFoods(hotelName, foodName, foodType, minPrice, maxPrice)).thenReturn(foodCourtDtoDisplays);
		 ResponseEntity<List<FoodCourtDtoDisplay>> responseEntity =customerApplicationController.getFoods(hotelName, foodName, foodType, minPrice, maxPrice);
		 assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
		 assertEquals(foodCourtDtoDisplays,responseEntity.getBody());
		 
		
	}

	@Test
	void testAddCart() {
		int customerid = 1;
		int foodid = 1;
		CartDto cartDto = CartDto.builder().paymentMethod("upi").quantity(1).build();
		ApiResponse apiResponse = ApiResponse.builder().status(201l).build();
		Mockito.when(customerApplicationService.addCarts(customerid, foodid, cartDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity= customerApplicationController.addCart(customerid, foodid, cartDto);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

	@Test
	void testAddPayment() {
		int customerid =1;
		ApiResponse apiResponse = ApiResponse.builder().status(201l).build();
		Mockito.when(customerApplicationService.addPayments(customerid)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity= customerApplicationController.addPayment(customerid);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

	@Test
	void testDeleteFoodcart() {
		int customerid =1;
		int foodid =1;
		ApiResponse apiResponse = ApiResponse.builder().status(201l).build();
		Mockito.when(customerApplicationService.deleteFoodcart(customerid, foodid)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity= customerApplicationController.deleteFoodcart(customerid, foodid);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

}
