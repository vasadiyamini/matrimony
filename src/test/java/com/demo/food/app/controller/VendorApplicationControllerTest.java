package com.demo.food.app.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.food.app.dto.ApiResponse;
import com.demo.food.app.dto.FoodCourtDto;
import com.demo.food.app.service.VendorApplicationService;
@ExtendWith(SpringExtension.class)

class VendorApplicationControllerTest {
	@InjectMocks 
	private VendorApplicationController vendorApplicationController;
	@Mock 
	private VendorApplicationService vendorApplicationService;
	@Test
	void testAddFoodCourt() {
		int vendorid =1;
		FoodCourtDto foodCourtDto =FoodCourtDto.builder().description("tasty food").foodName("Food").foodPrice(100).foodType("veg").build();
		ApiResponse apiresponse = ApiResponse.builder().status(201l).build();
		Mockito.when(vendorApplicationService.addFoodCou(vendorid, foodCourtDto)).thenReturn(apiresponse);
		ResponseEntity<ApiResponse> responseEntity = vendorApplicationController.addFoodCourt(vendorid, foodCourtDto);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

	@Test
	void testDeleteFoodCourt() {
		int vendorid =1;
		int foodid =1;
		ApiResponse apiresponse =ApiResponse.builder().status(201l).build(); 
		Mockito.when(vendorApplicationService.deleteFoodCou(vendorid, foodid)).thenReturn(apiresponse);
		ResponseEntity<ApiResponse> responseEntity = vendorApplicationController.deleteFoodCourt(vendorid, foodid);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

	@Test
	void testUpdateFoodCourt() {
		int vendorid =1;
        int foodid =1;
        FoodCourtDto foodCourtDto = FoodCourtDto.builder().description("tasty food").foodName("Food").foodPrice(100).foodType("veg").build();
		ApiResponse apiresponse =ApiResponse.builder().status(201l).build(); 
		Mockito.when(vendorApplicationService.updateFoodCou(vendorid, foodid, foodCourtDto)).thenReturn(apiresponse);
		ResponseEntity<ApiResponse> responseEntity = vendorApplicationController.updateFoodCourt(vendorid, foodid, foodCourtDto);
		assertEquals(201,responseEntity.getBody().getStatus());

	}

}
